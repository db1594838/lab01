--
-- File generated with SQLiteStudio v3.4.4 on จ. ก.ย. 18 14:50:39 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: EMPLOYEE
DROP TABLE IF EXISTS EMPLOYEE;

CREATE TABLE IF NOT EXISTS EMPLOYEE (
    ID_EM            INTEGER      PRIMARY KEY AUTOINCREMENT,
    ID_SALARY        INTEGER,
    EM_FULLNAME      VARCHAR (40) NOT NULL,
    EM_DATE_OF_BIRTH DATE,
    EM_TEL           CHAR (10),
    EM_EMAIL         VARCHAR (20),
    EM_ADDRESS       TEXT         NOT NULL,
    EM_JOB_POSITION  VARCHAR (20) NOT NULL,
    EM_BRANCH        VARCHAR (20) NOT NULL,
    EM_SALARY        SMALLINT     NOT NULL,
    FOREIGN KEY (
        ID_SALARY
    )
    REFERENCES SALARY
);


-- Table: MENU
DROP TABLE IF EXISTS MENU;

CREATE TABLE IF NOT EXISTS MENU (
    ID_MENU    INTEGER       PRIMARY KEY AUTOINCREMENT,
    MENU_NAME  VARCHAR (25)  NOT NULL,
    MENU_TYPE  VARCHAR (25)  NOT NULL,
    MENU_PRICE NUMBER (5, 2) NOT NULL
);

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     1,
                     'Apple Juice',
                     'Juice',
                     50
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     2,
                     'Mango Juice',
                     'Juice',
                     45
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     3,
                     'Tomato Juice',
                     'Juice',
                     45
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     4,
                     'Mango Juice',
                     'Juice',
                     45
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     5,
                     'Yellow Plum Juice',
                     'Juice',
                     45
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     6,
                     'Rosella Juice',
                     'Juice',
                     45
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     7,
                     'Cappuccino',
                     'coffee',
                     50
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     8,
                     'Mocca',
                     'coffee',
                     45
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     9,
                     'Latte',
                     'coffee',
                     45
                 );

INSERT INTO MENU (
                     ID_MENU,
                     MENU_NAME,
                     MENU_TYPE,
                     MENU_PRICE
                 )
                 VALUES (
                     10,
                     'Espresso',
                     'coffee',
                     50
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
